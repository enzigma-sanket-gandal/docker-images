FROM alpine:latest 
WORKDIR /App 

# Installing Kubernetes
ENV KUBE_LATEST_VERSION="v1.13.0"
RUN apk add --update ca-certificates \
    && apk add --update -t deps curl \
    && apk add --update gettext \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && apk del --purge deps \
    && rm /var/cache/apk/*

RUN apk add nodejs-npm

# Installing Azure CLI
RUN npm install -g azure-cli

# Installing Angular 
RUN npm install -g @angular/cli

# Installing Git 
RUN apk add git
